# Rust Server Voter
Logs in with Steam SSO, then votes for Jim Deadlock's Rust server.

## Dependencies
- Python3.5+
- Firefox
- `pip3 install -U selenium`
- `pip3 install -U webdriver-manager`

## Usage
To log-in run:
```sh
./main.py --login
```
It'll open a new Firefox window with Steam login prompt. Don't close that window after logging in! Instead go back to the terminal and press Enter.

Every next time just run:
```sh
./main.py
```
or use one of the examples below.

A `crontab` entry to run it in headless mode at midnight and noon with output logged to `~/tmp/rustvoter.log`:
```crontab
0 0,12 * * * ~/Projects/rust-voter/main.py 2>&1 1>~/tmp/rustvoter.log
```

Same with nice GUI notifications using `notify-send`:
```crontab
0 0,12 * * * export XDG_RUNTIME_DIR=/run/user/$(id -u); notify-send rust-voter "Voting"; ~/Projects/rust-voter/main.py 2>&1 1>~/tmp/rustvoter.log; notify-send rust-voter "$(tail -1 ~/tmp/rustvoter.log)"
```

Same without `cron` nor notifications nor logging, voting once every 12 hours:
```sh
while true; do ./main.py; sleep 43200; done
```

## Final notes
- session is saved in `~/tmp/rustvoter_cookies.pkl`
- I highly recommend using Python environment so that dependencies wouldn't confilct with global Python3 packages on the host system
