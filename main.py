#!/usr/bin/env python3
import argparse
import locale
import os
import time
import pickle
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.service import Service
from webdriver_manager.firefox import GeckoDriverManager

locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')

cookiejar = "~/tmp/rustvoter_cookies.pkl"

class RustVoter:

    def __init__(self, driver):
        global cookiejar
        self.__cookiejar = os.path.expanduser(cookiejar)
        self.__driver = driver

    def open_website(self):
        url = "https://rust-servers.net/server/156658/vote/"
        self.__driver.get(url)
        try:
            assert "Vote for " in self.__driver.title
            time.sleep(1) # TODO
            self.__driver.find_element(by=By.ID, value="cookiescript_accept").click()
        except AssertionError:
            raise NoPage()
        except NoSuchElementException:
            pass

    def vote(self):
        checkbox = self.__driver.find_element(by=By.ID, value="accept")
        checkbox.click()
        votebutton = self.__driver.find_element(by=By.CSS_SELECTOR, value="#vote-form-block > form > input[type=image]")
        votebutton.click()
        try:
            assert "Sign In" in self.__driver.title
            self.__set_cookies()
            self.__driver.refresh()
            self.__driver.find_element(by=By.ID, value="imageLogin").click()
        except NoSuchElementException:
            self.__clear_cookies()
            raise NoLoginCookie()
        try:
            assert "Vote confirmation for " in self.__driver.title
        except AssertionError:
            raise AlreadyVoted()

    def login(self):
        self.__driver.get("https://steamcommunity.com/login/home/")
        assert "Sign In" in self.__driver.title
        input()
        assert "Steam Community" in self.__driver.title
        self.__save_cookies()

    def quit(self, message = "Quitting...", save_screenshot = False, screenshot = "error.png"):
        if save_screenshot:
            self.__driver.save_screenshot(screenshot)
            message += " - check {}".format(screenshot)
        self.__driver.quit()
        print(message)
        raise SystemExit()

    def __save_cookies(self):
        pickle.dump(self.__driver.get_cookies(), open(self.__cookiejar, "wb"))

    def __set_cookies(self):
        try:
            for cookie in pickle.load(open(self.__cookiejar, "rb")):
                self.__driver.add_cookie(cookie)
        except IOError:
            self.__clear_cookies()
            raise NoLoginCookie()

    def __clear_cookies(self):
        try:
            os.remove(self.__cookiejar)
        except FileNotFoundError:
            pass

class NoPage(Exception):
    pass

class NoLoginCookie(Exception):
    pass

class AlreadyVoted(Exception):
    pass

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--login', action='store_true')
    args = parser.parse_args()
    options = Options()
    mode = "graphical"
    if not args.login:
        options.add_argument('-headless')
        mode = "headless"
    print("Setting " + mode + " mode...")
    print("Loading driver...")
    driver = webdriver.Firefox(service=Service(GeckoDriverManager().install()), options=options)
    rv = RustVoter(driver)
    try:
        print("Opening website...")
        rv.open_website()
        print("Voting...")
        rv.vote()
        rv.quit("Done")
    except NoPage:
        rv.quit("No page loaded", True)
    except NoLoginCookie:
        if not args.login:
            rv.quit("Run with `--login` to log into Steam.")
        print("Log into Steam and press enter...")
        rv.login()
        print("Login cookie saved.")
        rv.quit("Run again without --login")
    except AlreadyVoted:
        rv.quit("Already voted today")
